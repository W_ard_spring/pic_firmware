
                        				
                        
#include <stdio.h>            
#include <p30f5015.h> 

										
						
#define AzButton1   PORTCbits.RC13  	 	
#define AzButton2   PORTCbits.RC14 		
#define ElButton1   PORTFbits.RF4  	 	
#define ElButton2   PORTFbits.RF5   	 	
#define SysModSwitch  PORTGbits.RG6   	 	

#define AzLed1 	PORTFbits.RF0   	 		
#define AzLed2	PORTFbits.RF1  		 		
#define ElLed1	PORTGbits.RG2  		 		
#define ElLed2	PORTGbits.RG3 				

#define STD_PWM_PTPER	    24999     //1999(100us)/19999(1ms)/24999(1.25ms)/39999(2ms)    

#define MANUL_FWD_PDC1	    46000	  //3000(100us)/25000(1ms)/38000(1ms)/45000(1.25ms)	/70000(2ms)	
#define MANUL_BWD_PDC1		4000     //1000       /15000     /2000       /5000 			/10000	           
#define MANUL_FWD_PDC3		45000     //3000      /25000    /35000       /45000			/70000	              
#define MANUL_BWD_PDC3		5000     //1000       /15000     /5000       /5000			/10000	              
#define MaxLimitPDC1        49000     //39000(1ms)/76000(2ms)/47000(1.25ms)      //�����һ��溷 ��̡�� �� ʺ�� PDC�ٱ˰�
#define MinLimitPDC1        1000      //1000      /4000        /3000
#define MaxLimitPDC3        46000     //36000     /76000       /47000
#define MinLimitPDC3        4000      //4000      /4000        /3000	     	

#define	SLOW_EVENT_PERIOD	  80      //1000(100us)/100(1ms)/80(1.25ms)/50(2ms)  	
#define	MEDIUM_EVENT_PERIOD	  4	      //50         /5       /4         /2       	


				  						
_FOSC(CSW_FSCM_OFF & XT_PLL8);  			
_FWDT(WDT_OFF);                 			
_FBORPOR(PBOR_OFF & MCLR_EN);   			

										
						 
struct {								   
		unsigned 	AzDir:1;			  
        unsigned   	ElDir:1;		 	  
        unsigned   	AzBtn1:1;				
        unsigned   	AzBtn2:1;				
        unsigned   	ElBtn1:1;			
        unsigned   	ElBtn2:1;			
        unsigned    SysModSth:1;		  
        unsigned    PwmFaultBtn:1;			
        unsigned  	PWMFault:1;				
        unsigned  	Repeat:1;				
        unsigned  	ErrCheck:1;				
        unsigned    SetReceiveX:1;		    
        unsigned   	CompReceiveX:1; 		
        unsigned   	PWMEvent:1;		    	
        unsigned   	MediumEvent:1;      	
        unsigned    SlowEvent:1;        	
} Flags;
                						
                
unsigned int SlowEventCount, MediumEventCount, 
			 SetPDC1, SetPDC3;  		 	   

unsigned int PDC1_CHANGE_VALUE, PDC3_CHANGE_VALUE;	
unsigned short RxBuf[10];                      
unsigned char RxNum , MsgLen ;  		  	  

										
				
void Initialize(void);		            	
void Com_scan(void);                      
void Encoder_data_Transmit(void);        
void Delay(unsigned int);    				

unsigned char Getch(void);                
void Putch(unsigned short) ;               
void UARTClrError(void);

void __attribute__((__interrupt__)) _PWMInterrupt(void);  	
void __attribute__((__interrupt__)) _U1RXInterrupt(void);   

										
					
void main(void)
{
  Initialize();  							
 
  while(1)  								
  { 
	ClrWdt();									

	if (Flags.PWMEvent)					
	{	
	 	if (Flags.SetReceiveX)						
		{
			while((RxBuf[RxNum]!= 0xA2) && (RxNum < 9))	
	        {
		      	RxNum++ ;  
		      	RxBuf[RxNum] = Getch();
			}
			
			MsgLen = RxNum - 1;
			Flags.CompReceiveX = 1;			
			Flags.SetReceiveX = 0;			
		}
    
	  	Flags.PWMEvent = 0;             		
		
	}// end if(PWMEvent)

	if(Flags.MediumEvent)  	   			
	{

		if (PDC1 < SetPDC1)				    
		{                                   // ��̡��
			PDC1 += PDC1_CHANGE_VALUE;
			if (PDC1 > SetPDC1) PDC1 = SetPDC1;	
		}
	    if (PDC1 > SetPDC1)       //else
		{
			PDC1 -= PDC1_CHANGE_VALUE;
			if (PDC1 < SetPDC1) PDC1 = SetPDC1;	
		}	
		
		if (PDC3 < SetPDC3)                 // ʺ��
		{
			PDC3 += PDC3_CHANGE_VALUE;
			if (PDC3 > SetPDC3) PDC3 = SetPDC3;	
		}
	    if (PDC3 > SetPDC3)   	 //else
		{
			PDC3 -= PDC3_CHANGE_VALUE;
			if (PDC3 < SetPDC3) PDC3 = SetPDC3;	
		}

		if (Flags.CompReceiveX)				
		{
			Com_scan();         			                     
			Flags.CompReceiveX = 0;        
			_U1RXIE = 1;
		}


		Flags.MediumEvent = 0;          
	}// End if (MediumEvent)

	if (Flags.SlowEvent)	   			
	{
		if (AzButton1)	Flags.AzBtn1 = 1;   else  Flags.AzBtn1 = 0;    
		if (AzButton2)	Flags.AzBtn2 = 1;   else  Flags.AzBtn2 = 0;
		if (ElButton1)	Flags.ElBtn1 = 1;   else  Flags.ElBtn1 = 0;
		if (ElButton2)	Flags.ElBtn2 = 1;   else  Flags.ElBtn2 = 0;	
		if (SysModSwitch) Flags.SysModSth = 1; else  Flags.SysModSth = 0;//1:����/0:�㴴		
      	
		if (!Flags.SysModSth)     			
		{
			PDC1_CHANGE_VALUE = 500;   //100
            PDC3_CHANGE_VALUE = 400;    //60
			
			if (Flags.AzBtn1) 									
			{
				AzLed1 = 1;                
				SetPDC1 = MANUL_FWD_PDC1;
			}
			else 
			{
				if (!Flags.AzBtn2) {SetPDC1 = PTPER;  PDC1 = PTPER;}  
				AzLed1 = 0;                                            
			}

			if (Flags.AzBtn2) 							
			{
				AzLed2 = 1;                 //Delay(100000); AzLed2 = 0;
			}
			else 
			{
				if (!Flags.AzBtn1) {SetPDC1 = PTPER;  PDC1 = PTPER;}
				AzLed2 = 0;
			}

			if (Flags.ElBtn1) 							
			{
				ElLed1 = 1;                 //Delay(100000); ElLed1 = 0;(���Ŵ� ����)
				SetPDC3 = MANUL_FWD_PDC3;
			}
			else 
			{
				if (!Flags.ElBtn2) {SetPDC3= PTPER;PDC3 = PTPER;} 
				ElLed1 = 0;
			}

			if (Flags.ElBtn2)						
			{
				ElLed2 = 1;                //Delay(100000);  ElLed2 = 0;
			}
			else 
			{
				if (!Flags.ElBtn1) {SetPDC3= PTPER; PDC3 = PTPER;}  
				ElLed2 = 0;
			}
			
			if (!Flags.Repeat) Flags.Repeat = 1;
		}
		else								
		{	
			if (Flags.Repeat) 				
			{
				SetPDC1 = PTPER;
				SetPDC3 = PTPER;
				AzLed1 = 0; Nop();Nop();	
				AzLed2 = 0; Nop();Nop();	
				ElLed1 = 0; Nop();Nop();	
				ElLed2 = 0;  
				Flags.Repeat = 0;
			} 
		}// if (!Flags.SysModSth) 
	
		Flags.SlowEvent = 0;
		
 	} //end if (Flags.SlowEvent)
  }   // while(1) 	
}     // main

                   
void Initialize(void)					
{ 							
	  								     
	SlowEventCount = SLOW_EVENT_PERIOD;     // = (100msec)
	MediumEventCount = MEDIUM_EVENT_PERIOD; // = (5msec)
    PDC1_CHANGE_VALUE = 40;
    PDC3_CHANGE_VALUE = 60;
											
							                
    PORTB = 0;				
	PORTC = 0;
	PORTD = 0;
	PORTE = 0;
	PORTF = 0;
	PORTG = 0;
  						
	TRISB=0xFFFF;       
  						
	
	TRISC=0x6000;  		
	
	TRISD=0x0FFF;  		
	TRISE=0x00CC;       
	TRISF=0x0074;  		
	TRISG=0x03C0;   	
											
	ADPCFG = 0xFFFF;    
	
	
	PTPER = STD_PWM_PTPER; 
	PDC1 = PTPER; 
	PDC3 = PTPER;		
	PDC2 = 0;
	PDC4 = 0;
	
	SetPDC1 = PTPER; 
	SetPDC3 = PTPER;	


	PWMCON1 = 0x0055;   
	DTCON1 = 0xFFFF;  
						

	DTCON2 = 0x0022; 					
	
	OVDCON = 0x3300;    
	PTCON = 0x8000;		
	PWMCON2 = 0x0004;  	
	
	_PWMIF = 0;         
	_PWMIE = 1; 		
			
										
   	U1MODE = 0x8000;	
						
	U1STA = 0x0510;    
 						
    U1BRG = 129;  //64; //129;  		
////	_U1TXIE=1;  		
  	_U1TXIF=0;    

	_U1RXIE=1;  		
   	_U1RXIF=0;
 
    U1TXREG = 0;		
    
    CNEN1=0;CNEN2=0;    
    CNPU1=0;CNPU2=0;    
    _CNIE=0;           
    
     								
	AzLed1 = 1; Nop(); Nop();	
	AzLed2 = 1; Nop(); Nop();	
	ElLed1 = 1; Nop(); Nop();	
	ElLed2 = 1; Delay(1000);

	AzLed1 = 0; Nop(); Nop();	
	AzLed2 = 0; Nop(); Nop();	
	ElLed1 = 0; Nop(); Nop();	
	ElLed2 = 0; 	
}  // end Initialize()

unsigned char Getch(void)
{
	unsigned char Temp;
	while( _U1RXIF == 0);
	Temp = U1RXREG;
	_U1RXIF = 0;
	UARTClrError();
	return Temp;
}
void Putch(unsigned short byte)  //void Putch(BYTE byte)
{
	while (U1STAbits.UTXBF == 1); // Wait for Tx buffer is not full
	U1TXREG = byte;
}

void UARTClrError(void)		// Clear error flag
{
	if(U1STAbits.OERR) U1STAbits.OERR = 0;
}

void Com_scan()							
{
    unsigned short ChkSum = 0;
    unsigned int ii;
    
    Flags.ErrCheck = 0; 
    
    switch(RxBuf[1])        
	{
		case 10: if(RxNum == 4) Flags.ErrCheck = 1; break;
        
        case 30: if(RxNum == 9) Flags.ErrCheck = 1; break;
    }
	
	if(Flags.ErrCheck == 1)
    {  
		RxNum = 1;
		while (RxNum < MsgLen) 
		{
			ChkSum += RxBuf[RxNum];
			RxNum++;
		}   
	
		if (RxBuf[MsgLen] == (ChkSum & 0x7F))
		{
			switch (RxBuf[1])  					
			{
    			case 10: Encoder_data_Transmit();
    			        
    			         if (RxBuf[2] < 1) RxBuf[2] = 1;   
    			         if (RxBuf[2] > 100) RxBuf[2] = 100;
    			         
    			         if (Flags.SysModSth)
    			         {      			         
    			         	PDC1_CHANGE_VALUE = RxBuf[2] * 400;    //40  //200
    		             	PDC3_CHANGE_VALUE = RxBuf[2] * 200;    //20  //100            
    		          	 }   	
    		        	 break;
    		        
    			case 30: if (Flags.SysModSth)	
    		        	 {
	    		        	SetPDC1 = RxBuf[2]|(RxBuf[3]<<7)|(RxBuf[4]<<14);   
	    		                                                                
	    		                                                                
    		            	SetPDC3 = RxBuf[5]|(RxBuf[6]<<7)|(RxBuf[7]<<14);  	
    		                                                                  					        					       					        
						        
					       	if (SetPDC1 > PTPER) Flags.AzDir = 1;  else  Flags.AzDir = 0;				
							if (SetPDC3 > PTPER) Flags.ElDir = 1;  else  Flags.ElDir = 0;						 
							
							if(Flags.AzDir)                                 
							{ 
								AzLed1 =! AzLed1;  Nop(); Nop();           
								AzLed2 = 0;
							}   
							else 
							{ 
								AzLed1 = 0;  Nop(); Nop(); 
							    AzLed2 =! AzLed2;
							}
								
							if(Flags.ElDir) 
							{ 
								ElLed1 =! ElLed1;  Nop(); Nop();  
								ElLed2 = 0;
							}  
							else 
							{ 
								ElLed1 = 0;  Nop(); Nop();  
								ElLed2 =! ElLed2;
							}	
					       		        
					      	        
					        if (SetPDC1 > MaxLimitPDC1) SetPDC1 = MaxLimitPDC1;      
							if (SetPDC1 < MinLimitPDC1) SetPDC1 = MinLimitPDC1;
					    	if (SetPDC3 > MaxLimitPDC3) SetPDC3 = MaxLimitPDC3;
							if (SetPDC3 < MinLimitPDC3) SetPDC3 = MinLimitPDC3;
						/*
							if (SetPDC1 == 15000 && SetPDC3 == 10000)
							{
								Putch(0x0F);
							}
						*/					
               			}              				
				       	break;
      			
				case 50: //StopPWM();
    		        	break;
    			
				case 70: break;
    		}// end of switch   
  		}// end of if ChkSum
   	}
  
    for(ii = 0; ii < 10; ii++);
	{
		RxBuf[ii] = 0;
    }  // end of Flags.ErrCheck                   
	
	//U1MODEbits.UARTEN = 0;
	//Delay(100);
	//U1MODEbits.UARTEN = 1; 	
}// end of Com_scan 

void Encoder_data_Transmit(void)		
{
  	unsigned short Aport[10], E1port[10], E2port[10];
  	unsigned int ii, jj, kk1, kk2, kk3;
  	unsigned short temp_az, temp_el_D, temp_el_G;
  	unsigned char az_data_high, az_data_low, el_data_high, el_data_low;
  	unsigned short checksum;
 
    for(ii = 1; ii < 11; ii++)
  	{
	  	Aport[ii] = PORTB; Nop(); Nop();
	  	E1port[ii] = PORTD; Nop(); Nop();
	  	E2port[ii] = PORTG; Nop(); Nop();  	    
	  	//Delay(100); 
   
    for(ii = 1; ii < 5; ii++)
  	{
	  	kk1 = kk2 = kk3 = 0;
	  	for(jj = ii+1; jj < 11; jj++)
	  	{
		  
		  	if(Aport[ii] == Aport[jj])  { kk1++; }
		  	if(E1port[ii] == E1port[jj]) { kk2++; }
		  	if(E2port[ii] == E2port[jj]) { kk3++; }
	  	}	  	
	  	if((kk1>6) && (kk2>6) && (kk3>6)) 
	  	{
	  	   temp_az = Aport[ii]; 
	  	   temp_el_D = E1port[ii];
	  	   temp_el_G = E2port[ii];  break;
	  	} 	  		  	
  	} 		
    	
  //	temp_az = PORTB; Nop(); Nop(); //0xD309; //PORTB; Nop(); Nop();	
  //	temp_el_D = PORTD; Nop(); Nop(); //0x04CE; //PORTD; Nop(); Nop();
  //	temp_el_G = PORTG; Nop(); Nop();
	      
  	az_data_low = (temp_az & 0x000F) | ((temp_az>>4) & 0x0070);           
  	az_data_high = (temp_az>>11) & 0x001F;                            	
  	el_data_low = temp_el_D & 0x007F; 							      
  	el_data_high = ((temp_el_D>>7) & 0x0019) | ((temp_el_G>>6) & 0x0006);  
  	checksum=az_data_low+az_data_high+el_data_low+el_data_high;           
                     
  	Putch(0x8A); 		       
  	Putch(az_data_low);         
  	Putch(az_data_high);
  	Putch(el_data_low);
  	Putch(el_data_high); 	
  	Putch(checksum & 0x7F);  	
    Putch(0x8C);	          
	//Delay(20);			
}
void __attribute__((__interrupt__)) _PWMInterrupt(void)   
{
	SlowEventCount--;
	if(SlowEventCount == 0)
	{
		Flags.SlowEvent = 1;
		SlowEventCount = SLOW_EVENT_PERIOD;
	}
	MediumEventCount--;
	if(MediumEventCount == 0)
	{
		Flags.MediumEvent = 1;
		MediumEventCount = MEDIUM_EVENT_PERIOD;
	}
	Flags.PWMEvent = 1;
	_PWMIF = 0;
}
void __attribute__((__interrupt__)) _U1RXInterrupt(void)	
{
	RxNum = 0;
	RxBuf[0] = U1RXREG;

	if (RxBuf[0] == 0xA1)
	{
		Flags.SetReceiveX = 1;   
		_U1RXIE = 0;
	}
	_U1RXIF = 0;  
	UARTClrError();
}
void Delay(unsigned int count)							
{
	unsigned int j;

	for(j = 0; j<count; j++);
} // End of code  



/*
U1TXREG :  - - - -,  - - - UTX8,  Transmit Register (0000 000u uuuu uuuu)
		
U1RXREG :  - - - -,  - - - URX8,  Receive Register (0000 0000 0000 0000)
		
U1BRG  :	Baud Rate Generator Prescaler (0000 0000 0000 0000)                   	
		
*/

